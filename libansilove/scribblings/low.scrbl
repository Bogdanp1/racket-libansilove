#!/usr/bin/env racket


;; This file is part of racket-libansilove.

;; racket-libansilove is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-libansilove is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-libansilove.  If not, see <https://www.gnu.org/licenses/>.

;; Copyright (c) 2021-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later


#lang scribble/manual

@(require
   @(for-label
     racket/base
     libansilove/libansilove
     )
   )


@title[#:tag "libansilove-low"]{Libansilove Low-level bindings}

@defmodule[libansilove/libansilove]


@section{Structs}

@defthing[_ansilove_png ctype?]
@defstruct[
 ansilove_png
 ([*buffer number?] [length number?])
 #:omit-constructor
 ]

@defthing[_ansilove_ctx ctype?]
@defstruct[
 ansilove_ctx
 ([*buffer  number?]
  [maplen   number?]
  [length   number?]
  [png      ansilove_png]
  [error    number?])
 #:omit-constructor
 ]

@defthing[_ansilove_options ctype?]
@defstruct[
 ansilove_options
 ([diz           boolean?]
  [dos           boolean?]
  [icecolors     boolean?]
  [truecolor     boolean?]
  [columns       number?]
  [font          number?]
  [bits          number?]
  [mode          number?]
  [scale_factor  number?])
 #:omit-constructor
 ]


@section{Functions}

@defproc[(ansilove_error
          [ctx      ansilove_ctx]
          )
         string?
         ]{}

@defproc[(ansilove_init
          [ctx      ansilove_ctx]
          [options  ansilove_options]
          )
         void?
         ]{}

@defproc[(ansilove_loadfile
          [ctx      ansilove_ctx]
          [string   string?]
          )
         void?
         ]{}

@defproc[(ansilove_ansi
          [ctx      ansilove_ctx]
          [options  ansilove_options]
          )
         void?
         ]{}

@defproc[(ansilove_artworx
          [ctx      ansilove_ctx]
          [options  ansilove_options]
          )
         void?
         ]{}

@defproc[(ansilove_binary
          [ctx      ansilove_ctx]
          [options  ansilove_options]
          )
         void?
         ]{}

@defproc[(ansilove_icedraw
          [ctx      ansilove_ctx]
          [options  ansilove_options]
          )
         void?
         ]{}

@defproc[(ansilove_pcboard
          [ctx      ansilove_ctx]
          [options  ansilove_options]
          )
         void?
         ]{}

@defproc[(ansilove_tundra
          [ctx      ansilove_ctx]
          [options  ansilove_options]
          )
         void?
         ]{}

@defproc[(ansilove_xbin
          [ctx      ansilove_ctx]
          [options  ansilove_options]
          )
         void?
         ]{}

@defproc[(ansilove_savefile
          [ctx      ansilove_ctx]
          [string   string?]
          )
         void?
         ]{}

@defproc[(ansilove_clean
          [ctx      ansilove_ctx]
          )
         void?
         ]{}


@section{Placeholders}

@defproc[(ansilove_ctx-placeholder)
         (ansilove_ctx 0 0 0 (ansilove_png 0 0) 0)
         ]{Produces a default @racket[ansilove_ctx] @racket[struct].}

@defproc[(ansilove_options-placeholder)
         (ansilove_options #f #f #f #f 80 1 8 1 1)
         ]{Produces a default @racket[ansilove_options] @racket[struct].}
